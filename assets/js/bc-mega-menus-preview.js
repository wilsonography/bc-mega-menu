(function( wp, $ ){
	if ( ! wp || ! wp.customize ) { return; }

	var api = wp.customize

	api.BCMMPreview = {
		init: function () {
			this.clearControlFocus();
		},

		/**
		 * Removes the default on hover title and clears the default shift+click
		 * event used to focus the widget control.
		 */
		clearControlFocus: function() {
			var selector = '.bcmm-mega-menu .widget';

			$( selector ).removeAttr( 'title' );
			$( selector ).on( 'click', function() {
				return false;
			} );
		}
	};

	$(function () {
		api.BCMMPreview.init();
	});
})( window.wp, jQuery );