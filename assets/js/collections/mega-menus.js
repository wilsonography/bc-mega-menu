( function( wp, $ ) {

	'use strict';

	if ( ! wp || ! wp.customize ) { return; }

	// Set up our namespace.
	var api = wp.customize;

	api.BCMM = api.BCMM || {};

	/**
	 * wp.customize.BCMM.MegaMenusCollection
	 *
	 * Collection for mega menus models.
	 *
	 * @constructor
	 * @augments Backbone.Model
	 */
	api.BCMM.MegaMenusCollection = Backbone.Collection.extend({
		model: api.BCMM.MegaMenuModel
	});

} )( window.wp, jQuery );