( function( wp, $ ) {

	'use strict';

	if ( ! wp || ! wp.customize ) { return; }

	// Set up our namespace.
	var api = wp.customize;

	api.BCMM = api.BCMM || {};

	/**
	 * wp.customize.BCMM.WidgetsCollection
	 *
	 * Collection for widget models.
	 *
	 * @constructor
	 * @augments Backbone.Model
	 */
	api.BCMM.WidgetsCollection = Backbone.Collection.extend({
		model: api.BCMM.WidgetModel
	});

} )( window.wp, jQuery );