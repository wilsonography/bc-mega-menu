<?php

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

// Version.
delete_option( 'bc-mega-menus-version' );

// Remove BCMM data.
delete_option( 'BCMM_DATA' );
